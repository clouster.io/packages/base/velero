#!/usr/bin/env bash
set -euo pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

prepare() {
  chart_folder="$1"
  chart_name="$2"
  chart_description="$3"
  chart_version="$4"
  app_version="${5:-$(yq .appVersion "$chart_folder/Chart.yaml")}"

  echo "Creating Chart.yaml"
  echo "chart name: $chart_name"
  echo "chart description: $chart_description"
  echo "chart version: $chart_version"
  echo "app version: $app_version"

  yq eval -i ".name |= \"$chart_name\" | .description |= \"$chart_description\" | .version |= \"$chart_version\" | .appVersion |= \"$app_version\"" "$chart_folder/Chart.yaml"

  patch_dir="$SCRIPT_DIR/patch"

  if [ -d "$patch_dir" ]; then
    find "$patch_dir" -type f -name '*.diff' -printf '%P\0' | while read -d $'\0' p
    do
        local target="$chart_folder/${p%.diff}"
        echo "Applying patch $p";
        patch "$target" "$patch_dir/$p"
    done

    find "$patch_dir" -type f -not -name '*.diff' -printf '%P\0' | while read -d $'\0' p
    do
        local target="$chart_folder/${p}"
        echo "Copying file $p";
        cp "$patch_dir/$p" "$target"
    done
  fi

  touch /root/ips.values
  if [ -n "$IMAGE_PULL_SECRET" ]; then
    yq eval -iP 'env(IMAGE_PULL_SECRET) as $ps | .image.imagePullSecrets[0].name |= $ps' /root/ips.values
  fi
}

write-values() {
  output_file="$1"
  touch "$output_file"
  shift

  while [[ $# -gt 0 ]]; do
    key_path="$1"
    value="$2"

    echo "Writing value to key: $key_path"
    if [ -n "$value" ]; then
      value="$value" yq -Pi ".$key_path |= env(value)" "$output_file"
    else
      yq -Pi ".$key_path |= \"\"" "$output_file"
    fi

    shift
    shift
  done
}

install-crds() {
  chart_folder="$1"
  crd_folder="$chart_folder/crds"

  kubectl apply -f "$crd_folder"
}

delete-crds() {
  chart_folder="$1"
  crd_folder="$chart_folder/crds"

  kubectl delete -f "$crd_folder"
}

# Call the requested function and pass the arguments as-is
"$@"
